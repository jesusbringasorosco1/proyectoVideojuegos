/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Estudiante
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Pantalla extends JPanel implements KeyListener{
    private Helicoptero Helicoptero1 = new Helicoptero(10, 10, 10, "helicoptero.png");
    private Helicoptero Helicoptero2 =new Helicoptero(250,40, 10, "helicoptero.png");
    private Helicoptero Helicoptero3 =new Helicoptero(500,80, 10, "helicoptero.png");
    private Tanque Tanque1=new Tanque(10, 350 ,"Tanque.png");
    private Bala bala1=new Bala(100,150,3);
    public int el1=0,el2,el3=0;



    private Timer timer;

    public Pantalla(Dimension d) {
        this.setSize(d);
        this.setPreferredSize(d);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.setBackground(new Color(0, 128, 0));
        Helicoptero1.LimitesXY(getWidth(), getHeight());        
        Helicoptero2.LimitesXY(getWidth(), getHeight());
        Helicoptero3.LimitesXY(getWidth(), getHeight());
        Tanque1.LimitesXY(getWidth(), getHeight());
        bala1.LimitesXY(getWidth(), getHeight());
        
        //para la animacion
        timer = new Timer(16, new ActionListener () {
            public void actionPerformed(ActionEvent a) {
                Helicoptero1.move();
                Helicoptero2.move();
                Helicoptero3.move();
                bala1.move();
                
                if (Helicoptero1.Colision(bala1.ObtenerX1(), bala1.ObtenerY1(), bala1.ObtenerBase(),bala1.ObtenerAltura() , bala1.ObtenerEstado())) {
                     JOptionPane.showMessageDialog(null, "Choco Helicoptero 1");
                     bala1.SetEstado(false);
                     el1++;
                     Helicoptero1.cargar(el1,el2,el3);
                }
                
                
                if (Helicoptero2.Colision(bala1.ObtenerX1(), bala1.ObtenerY1(), bala1.ObtenerBase(),bala1.ObtenerAltura() , bala1.ObtenerEstado())) {
                     JOptionPane.showMessageDialog(null, "Choco Helicoptero 2");
                     bala1.SetEstado(false);
                     el2++;
                     Helicoptero1.cargar(el1,el2,el3);
                }
                
                if (Helicoptero3.Colision(bala1.ObtenerX1(), bala1.ObtenerY1(), bala1.ObtenerBase(),bala1.ObtenerAltura() , bala1.ObtenerEstado())) {
                     JOptionPane.showMessageDialog(null, "Choco Helicoptero 3");
                     bala1.SetEstado(false);
                     el3++;
                     Helicoptero1.cargar(el1,el2,el3);
                }
                //Tanque1.move();
                repaint();
            }
        });
    }
    
     public void animar(boolean turnOnOff) {
        if (turnOnOff) {
            Helicoptero1.setVelocidadXY();
            Helicoptero2.setVelocidadXY();
            Helicoptero3.setVelocidadXY();
            bala1.setVelocidadXY();
            //Tanque1.setVelocidadXY();
            timer.start();
        } else {
            timer.stop();
        }
    }
    //Pinta la animacion
    //@Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        Helicoptero1.dibujar(g,1);
        Helicoptero1.dibujar1(g);
        Helicoptero2.dibujar(g,2);
        Helicoptero3.dibujar(g,3);        
        Tanque1.dibujar(g);
        bala1.dibujar(g);

    }
    
    public void tanquearriba(){
        //JOptionPane.showMessageDialog(null, "Arriba");
        Tanque1.TanqueArriba();
        repaint();
    }
    
    public void tanqueAbajo(){
        Tanque1.TanqueAbajo();
        repaint();
    }
    
    public void tanqueIzquierda(){
        Tanque1.TanqueIzquierda();
        repaint();
    }
    
    public void tanqueDerecha(){
        Tanque1.TanqueDerecha();
        repaint();
    }
    
    public void DisparaTanque(){
        
        bala1.Dispara(Tanque1.XTanque(),Tanque1.YTanque());
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         //switch (month){
        int keyCode = e.getKeyCode();
        switch( keyCode ) { 
            case KeyEvent.VK_UP:
                // handle up 
                this.tanquearriba();
                break;
            case KeyEvent.VK_DOWN:
                // handle down 
                this.tanqueAbajo();
                break;
            case KeyEvent.VK_LEFT:
                // handle left
                this.tanqueIzquierda();
                break;
            case KeyEvent.VK_RIGHT :
                // handle right
                this.tanqueDerecha();
                break;
            case KeyEvent.VK_SPACE :
                // handle right
                this.DisparaTanque();
                
                break;
         }
             
         
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
   
}
